    1  clear
    2  sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
    3  sudo apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654
    4  sudo apt-get update
    5  sudo apt-get install ros-kinetic-desktop-full
    6  apt-cache search ros-kinetic
    7  sudo rosdep init
    8  rosdep update
    9  echo "source /opt/ros/kinetic/setup.bash" >> ~/.bashrc
   10  source ~/.bashrc
   11  echo "source /opt/ros/kinetic/setup.zsh" >> ~/.zshrc
   12  source ~/.zshrc
   13  ls -al
   14  gedit .zshrc 
   15  source .zshrc 
   16  sudo apt install python-rosinstall python-rosinstall-generator python-wstool build-essential
   17  printenv | grep ROS
   18  source /opt/ros/kinetic/setup.bash
   19  mkdir -p ~/catkin_ws/src
   20  cd ~/catkin_ws/
   21  catkin_make
   22  source devel/setup.bash
   23  echo $ROS_PACKAGE_PATH
   24  rosdep update
   25  cd ~/catkin_ws/src
   26  wstool init
   27  wstool merge https://raw.github.com/knowrob/knowrob/master/rosinstall/knowrob-tutorial.rosinstall
   28  wstool update
   29  rosdep install --ignore-src --from-paths .
   30  rm -rf knowrob_tutorials/
   31  cd knowrob/
   32  ls
   33  git checkout 7e6c0732a18a138cf3ac752be9c5384f7a53f6ad
   34  cd ..
   35  rosdep install --ignore-src --from-paths .
   36  cd ~/catkin_ws
   37  catkin_make
   38  gedit ~/.bash
   39  gedit ~/.bashrc 
   40  gedit ~/.plrc
   41  source ~/.bashrc 
   42  sudo apt-get install firefox
   43  cd ..
   44  sudo apt-get remove docker docker-engine docker-ce docker-ce-cli docker.io
   45  sudo apt-get update
   46  sudo apt-get install     apt-transport-https     ca-certificates     curl     software-properties-common
   47  sudo apt-get remove docker docker-engine docker.io containerd runc
   48  sudo apt-get update
   49  sudo apt-get install     apt-transport-https     ca-certificates     curl     gnupg-agent     software-properties-common
   50  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
   51  sudo apt-key fingerprint 0EBFCD88
   52  sudo add-apt-repository    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   53     $(lsb_release -cs) \
   54     stable"
   55  sudo apt-get update
   56  sudo apt-get install docker-ce docker-ce-cli containerd.io
   57  apt-cache madison docker-ce
   58  sudo apt-get install docker-ce=5:18.09.1~3-0~ubuntu-xenial docker-ce-cli=5:18.09.1~3-0~ubuntu-xenial containerd.io
   59  git clone --recursive https://github.com/knowrob/docker.git
   60  cd docker
   61  sudo ./scripts/build all
   62  cd ./nginx-proxy/certs/
   63  ./gencert.sh
   64  docker rm -f nginx
   65  sudo docker rm -f nginx
   66  cd ..
   67  docker rm -f nginx
   68  sudo docker rm -f nginx
   69  gedit ~/.bashrc 
   70  ls -al
   71  cd ..
   72  ls -al
   73  gedit .bashrc 
   74  source .bashrc 
   75  cd docker/
   76  ./scripts/start-webrob
   77  ./scripts/stop-webrob 
   78  gedit .bashrc 
   79  cd ..
   80  gedit .bashrc 
   81  '/home/bank21235/docker/episode_data' '/home/bank21235/docker/episodes' 
   82  cd docker/
   83  sudo ./scripts/start-webrob
   84  docker rm $(docker ps -a -q)
   85  sudo su
   86  sudo ./scripts/stop-webrob
   87  sudo su
   88  sudo docker image prune
   89  sudo docker volume prune
   90  sudo docker rm episode_data
   91  gedit .bashrc 
   92  cd catkin_ws/
   93  source devel/setup.bash 
   94  cd ..
   95  source .bashrc 
   96  source .zshrc 
   97  cd docker
   98  sudo ./scripts/build nocache all
   99  sudo ./scripts/start-webrob
  100  sudo ./scripts/stop-webrob
  101  sudo docker rm episode_data
  102  sudo ./scripts/start-webrob
  103  sudo ./scripts/stop-webrob
  104  cd flask/
  105  docker build --no-cache -t openease/flask
  106  docker build openease/flask
  107  docker build flask
  108  ls
  109  cd ..
  110  docker build --no-cache -t openease/flask
  111  docker build --nocache -t openease/flask
  112  docker build --no-cache -t openease/flask
  113  docker build --no-cache -flask
  114  cd flask/
  115  docker build --no-cache -t openease/flask
  116  sudo docker build --no-cache -t openease/flask
  117  docker build --no-cache -t openease/flask .
  118  sudo docker build --no-cache -t openease/flask .
  119  cd ..
  120  sudo su
  121  docker image prune
  122  sudo docker image prune
  123  sudo docker volumn prune
  124  sudo docker volume prune
  125  sudo ./scripts/build nocache all
  126  sudo ./scripts/start-webrob
  127  cd ~/docker/webapps/easeapp
  128  gedit .bashrc 
  129  cd docker/
  130  sudo ./scripts/build nocache all
  131  ls
  132  ./scripts/st
  133  sudo ./scripts/stop-webrob 
  134  docker rm episode_data
  135  sudo docker rm episode_data
  136  sudo ./scripts/build nocache all
  137  cd flask
  138  docker build --no-cache -t openease/flask .
  139  sudo chmod ugo+rw /var/run/docker.sock
  140  docker build --no-cache -t openease/flask .
  141  cd ../
  142  ./scripts/stop-webrob 
  143  docker rm nginx docker-gen 
  144  ./scripts/start-webrob
  145  ./scripts/stop-webrob 
  146  docker rm nginx docker-gen 
  147  cd flask/
  148  docker build --no-cache -t openease/flask .
  149  cd ../
  150  ./scripts/start-webrob
  151  ./scripts/stop-webrob
  152  docker rmi openease/flask:latest 
  153  docker rmi openease/flask
  154  docker load
  155  docker load --help
  156  cd flask/
  157  docker build --no-cache -t openease/flask .
  158  cd ../
  159  ./scripts/start-webrob
  160  gedit .bashrc 
  161  echo $OPENEASE_EPISODE_DATA
  162  cd /home/bank21235/docker/episodes
  163  ls
  164  cd docker/
  165  sudo ./scripts/start-webrob
  166  sudo ./scripts/stop-webrob
  167  sudo docker rm episode_data
  168  sudo ./scripts/start-webrob
  169  echo $OPENEASE_EPISODE_DATA
  170  sudo ./scripts/stop-webrob
  171  sudo docker rm episode_data
  172  sudo ./scripts/start-webrob
  173  cd flask/
  174  docker build --no-cache -t openease/flask .
  175  sudo docker build --no-cache -t openease/flask .
  176  cd ..
  177  ./scripts/build
  178  sudo ./scripts/build
  179  sudo ./scripts/build knowrob
  180  sudo ./scripts/start-webrob
  181  sudo ./scripts/stop-webrob
  182  sudo docker rm episode_data
  183  sudo ./scripts/start-webrob
  184  sudo ./scripts/stop-webrob
  185  sudo docker rm episode_data
  186  sudo ./scripts/build
  187  sudo ./scripts/build knowrob
  188  sudo ./scripts/build nocache all
  189  sudo ./scripts/start-webrob
  190  sudo ./scripts/stop-webrob
  191  gedit .bashrc 
  192  cd ..
  193  gedit .bashrc 
  194  sudo docker rm episode_data
  195  cd docker
  196  sudo docker rm episode_data
  197  sudo ./scripts/start-webrob
  198  sudo ./scripts/stop-webrob
  199  sudo ./scripts/start-webrob
  200  sudo ./scripts/stop-webrob
  201  sudo su
  202  cd docker/
  203  ls
  204  cd kinetic/
  205  ls
  206  cd kinetic-
  207  cd kinetic-knowrob-daemon/
  208  ls
  209  gedit Dockerfile 
  210  cd docker/
  211  ls
  212  cd flask/
  213  ls
  214  gedit Dockerfile 
  215  cd ../..
  216  cd docker/
  217  cd kinetic/
  218  cd kinetic-knowrob-daemon/
  219  ls
  220  gedit Dockerfile 
  221  cd ..
  222  cd ../..
  223  history >> bank.txt
